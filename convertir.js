$(document).ready(function(){
  $(".submitForm").on("submit",function(event) {
    //Stop form from submitting normally
    event.preventDefault();
    let valor = $('#valor').val();
    const apiKey = '';
    let config = {
      method:'GET',
      url: `https://v6.exchangerate-api.com/v6/${apiKey}/pair/USD/MXN/`+valor
    }

    Swal.fire({
      title: `Quieres conocer la conversion de ${valor} USD a MXN?`,
      showCancelButton: true,
      cancelButtonText:'No',
      confirmButtonText: "Si",
      denyButtonText: `Don't save`
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        axiosRequest(config)
        .then(response => {
          response = response.data;
          let message = '';
          if(response.result === 'success') {
            message = `El dolar esta a  ${response.conversion_rate}, por cambiar ${valor} USD a MXN es ${response.conversion_result}`
          } else {
            message = 'Lo sentimos, no fue posible hacer la conversion.';
          }
          Swal.fire(message, "", "success");
        })
        .catch(error => {
          console.log(error);
        });
      } else if (result.isDenied) {
        Swal.fire("Changes are not saved", "", "info");
      }
    });
    

  });
});


let axiosRequest = async(config) => {

  return await axios(config);
}